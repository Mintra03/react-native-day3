import React, {Component} from 'react'
import { View, Text, Alert} from 'react-native'
import { Link } from 'react-router-native'

class Screen1 extends Component {
    UNSAFE_componentWillMount() {
        console.log(this.props)
        if (this.props.location && this.props.location.state && this.props.location.state.mynumber) {
            Alert.alert('Your number is',this.props.location.state.mynumber + '')
        }

    }
    
    render() {
        return (
            <View style={{flex: 1, backgroundColor: 'pink'}}>
            <Text style={{color:'black'}}> Screen 1 </Text>

            <Link to='/S2'>
                <Text style={{color: 'black'}}> Go to Screen2 </Text>
            </Link>
            </View>
        )
    }
}
export default Screen1