import React, {Component} from 'react'
import { View, Text } from 'react-native'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import Pageinput from './pageinput'
import Profile from './Profile'

class Router extends Component {
    render() {
        return (
            <NativeRouter>
                <Switch>
                    <Route excat path='/pageinput' component={Pageinput}/>
                    <Route excat path='/Profile' component={Profile}/>
                    <Redirect to='/pageinput'/>
                    </Switch>
                    </NativeRouter>

        )
    }
}
export default Router