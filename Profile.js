import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Button } from 'react-native';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu',
});

class Profile extends Component {

    goToScreen1 = () => {
        this.props.history.push('./S1')
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)

    }

    render() {
        return (
            <View style={styles.container} >
                <View style={styles.header}>
                        <View style={[styles.boxIcon, styles.center]}>
                            <Text style={styles.headertext}> {"<"} </Text>
                        </View>

                        <View style={[styles.boxheader, styles.center]}>
                            <Text style={styles.headertext}> My Profile </Text>
                        </View>
                    </View>

                <View style={[styles.content, styles.center]}>
                    <Text style={styles.headertext}> Username </Text>
                    <Text> {this.props.location.state.Username} </Text>
                    <Text style={styles.headertext}> First name </Text>
                    <Text style={styles.headertext}> Last name </Text>
                </View>

                <View style={[styles.footer, styles.center]}>
                    <Button title='Edit' onPress= {this.goToScreen1} />
                </View>

            </View>


        );
    }
}
export default Profile

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
    },
    boxheader: {
        backgroundColor: '#CFEE85',
        margin: 3,
        flexDirection: 'row',
        flex: 1,
    },
    boxIcon: {
        backgroundColor: '#CFEE85',
        margin: 5,
        flex: 0,
    },
    header: {
        backgroundColor: 'white',
        alignItems: 'center',
        flexDirection: 'row',
        margin: 3,
    },
    headertext: {
        color: 'black',
        fontSize: 20,
        fontWeight: 'bold',
        padding: 30
    },
    content: {
        backgroundColor: '#A5F1CD',
        flex: 1,
        flexDirection: 'column'
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    
    boxfooter: {
        backgroundColor: 'green',
        marginHorizontal: 3,
        flexDirection: 'row',
        flex: 1,
    },
    footer: {
            backgroundColor: '#A5F1CD',
            alignItems: 'center',
            flexDirection: 'row',
            margin: 0.6,
        },

});
