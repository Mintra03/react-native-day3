import React, {Component} from 'react'
import { View, Text, Button } from 'react-native'

class S2 extends Component {
    goToScreen1 = () => {
        this.props.history.push('./S1', {
            mynumber: 20
        })
    }
    render() {
        return (
            <View style={{flex: 1, backgroundColor: '#74E198'}}>
            <Text style={{color:'black'}}> Screen 2 </Text>
            
            <Button title='Back to Screen1 !!' onPress={this.goToScreen1} />
            </View>
        )
    }
}
export default S2